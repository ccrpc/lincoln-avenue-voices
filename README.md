# LRTP Input Map

Interactive input map for the [Placeholder](https://lrtp.cuuats.org/) using [Web Map
GL](https://gitlab.com/ccrpc/webmapgl).

## Building

This project is hosted via gitlab pages, the CI/CD is set up to build those pages whenever something is pushed to either the master branch or the create-pages branch.

## License

LRTP Input Map is available under the terms of the [BSD 3-clause
license](https://gitlab.com/ccrpc/lincoln-avenue-voices/blob/master/LICENSE.md).
