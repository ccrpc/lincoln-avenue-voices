import { h, Component, Prop, Event, EventEmitter, Host } from "@stencil/core";
import { _t } from "../i18n/i18n";
import { Feature, Point } from "geojson";

const monthNames = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

@Component({
  styleUrl: "comment-detail.css",
  tag: "lrtp-comment-detail",
})
export class CommentDetail {
  /**
   * Whether the user is allowed to like (or un-like) comments
   */
  @Prop() readonly allowLike: boolean = true;

  /**
   * The Features API url for liking and un-liking comments
   */
  @Prop() readonly likeUrl: string;

  /**
   * The GeoJSON feature relating to this comment.
   */
  @Prop({ mutable: true }) feature: Feature<Point>;

  /**
   * Some things (such as gl-popup) will pass along a list of features with
   * one item it in rather than setting the "feature" property. This is here to handle that scenario
   * and extract the feature from it.
   */
  @Prop() readonly features: Feature<Point>[];

  /**
   * The authentication token to communicate with the Features API
   */
  @Prop() readonly token: string;

  /**
   * Whether or not to display the like and locate buttons.
   * TODO This is currently called 'popup' as it references whether this comment is in a popup or not. Consider renaming to something like 'include buttons'
   */
  @Prop() readonly popup: boolean = false;

  /**
   * The id of the currently selected comment (for the purposes of highlighting in the gl-feature-list)
   */
  @Prop() readonly selectedId: number;

  /**
   * Emitted when the use chooses a comment as the 'selected' comment.
   */
  @Event() commentSelectedEvent: EventEmitter<Feature<Point>>;

  /**
   * If this component is added via the gl-feature-list the the property feature will be defined.
   * That said if instead this component is added via gl-popup, the property features will be defined and feature undefined.
   * So in the case we are given a list of features, get the first one.
   */
  componentWillLoad() {
    if (this.features != undefined && this.feature == undefined)
      this.feature = this.features[0];
    if (this.feature.properties.comment_description === "null")
      delete this.feature.properties.comment_description;
  }

  private doLocate() {
    if (
      this.feature.geometry == undefined ||
      this.feature.geometry.coordinates?.length !== 2
    )
      return;
    let mapEl = document.querySelector("gl-map");
    mapEl.map.flyTo({
      center: this.feature.geometry.coordinates as [number, number], // We check length === 2 in if statement so this is safe.
      zoom: 18,
    });
  }

  private doLocateComment = () => this.doLocate();
  private doSelectComment = () => {
    if (this.popup) {
      this.commentSelectedEvent.emit(this.feature);
    }
  };

  render() {
    const small = screen.width <= 640;
    const props = this.feature.properties;
    const mode = props.comment_mode;
    const modeImage = "/lincoln-avenue-voices/public/img/" + mode + ".png";
    const comment = (
      _t(`lrtp.form.${mode}.label`) +
      ": " +
      _t(`lrtp.form.${props.comment_type}`)
    ).replace(/\([^)]*\)/g, "");

    let dateStr = "";
    let date = new Date(this.feature.properties._created);
    if (!isNaN(date.getTime())) {
      dateStr =
        `${monthNames[date.getMonth()]} ${date.getDate()}, ` +
        `${date.getFullYear()}`;
      if (props.comment_description) dateStr += ":";
    }

    let content = [];
    if (!small)
      content.push(
        <ion-avatar slot="start">
          <img src={modeImage} alt={mode} />
        </ion-avatar>
      );

    content.push(
      <ion-label text-wrap>
        <h2>{comment}</h2>
        <p>
          <strong>{dateStr}</strong> {props.comment_description || null}
        </p>
      </ion-label>
    );

    if (!this.popup) {
      content.push(
        <ion-button
          fill="clear"
          slot="end"
          class="lrtp-locate-button"
          onClick={this.doLocateComment}
        >
          <ion-icon slot="icon-only" name="locate"></ion-icon>
        </ion-button>
      );

      content.push(
        <gl-like-button
          slot="end"
          url={this.likeUrl}
          token={this.token}
          feature={this.feature}
          disabled={!this.allowLike}
        ></gl-like-button>
      );
    }

    return (
      <Host class={`lrtp-mode-${this.feature.properties.comment_mode}`}>
        <ion-item
          lines={this.popup ? "none" : "full"}
          color={
            this.selectedId && this.selectedId === this.feature.id
              ? "secondary"
              : ""
          }
          id={
            this.selectedId && this.selectedId === this.feature.id
              ? "selectedComment"
              : ""
          }
          onClick={this.doSelectComment}
        >
          {content}
        </ion-item>
      </Host>
    );
  }
}
