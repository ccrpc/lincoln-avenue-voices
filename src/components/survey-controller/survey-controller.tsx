import { Component, Listen, Method, Prop } from "@stencil/core";
import { alertController } from "@ionic/core";
import { _t } from "../i18n/i18n";

@Component({
  tag: "lrtp-survey-controller",
})
export class SurveyController {
  private comments: number = 0;
  private likes: number = 0;
  private prompted: boolean = false;

  /**.
   * How many comments the user makes before they are prompted them to take the survey.
   *
   * If the user submits comments >= the threshold OR likes a number of comments >= the like threshold,
   * they will be prompted to take the survey
   */
  @Prop() readonly commentThreshold: number = 1;

  /**
   * How many likes the users leaves before they are prompted to take the survey.
   *
   * If the user submits comments >= the threshold OR likes a number of comments >= the like threshold,
   * they will be prompted to take the survey
   */
  @Prop() readonly likeThreshold: number = 2;

  /**
   * How long, in milliseconds, after the user has surpassed either threshold before they are prompted to take the survey.
   * If the user exceeds both thresholds within this time they will still only be prompted once.
   */
  @Prop() readonly delay: number = 3000;

  async componentWillLoad() {
    this.prompted = localStorage.getItem("lrtp.survey.prompted") === "true";
  }

  @Listen("body:glFeatureAdd")
  onFeatureAdd(e: CustomEvent) {
    if (e.detail.success) {
      this.comments += 1;
      this.onEvent(this.comments, this.commentThreshold);
    }
  }

  @Listen("body:glLike")
  onLike(e: CustomEvent) {
    if (e.detail.success) {
      this.likes += 1;
      this.onEvent(this.likes, this.likeThreshold);
    }
  }

  private onEvent(count: number, threshold: number) {
    if (count >= threshold && !this.prompted) {
      this.prompted = true;
      localStorage.setItem("lrtp.survey.prompted", "true");
      setTimeout(async () => {
        const alert = await this.create();
        return await alert.present();
      }, this.delay);
    }
  }

  /**
   * Creates the ionic alert controller which will open the alert which prompts the user to take the survey
   * @returns {Promise<HTMLIonAlertElement>} A promise to the ionic alert controller which will handle the survey prompt
   */
  @Method()
  create(): Promise<HTMLIonAlertElement> {
    this.prompted = true;
    localStorage.setItem("lrtp.survey.prompted", "true");

    return alertController.create({
      header: _t("lrtp.survey-controller.title"),
      message: _t("lrtp.survey-controller.text", {
        icon: '<ion-icon name="bulb"></ion-icon>',
      }),
      buttons: [
        {
          text: _t("lrtp.survey-controller.later"),
          role: "cancel",
          cssClass: "secondary",
        },
        {
          text: _t("lrtp.survey-controller.now"),
          handler: () => {
            window.open(document.querySelector("lrtp-app").surveyUrl, "_blank");
            // window.location.href =
            //   document.querySelector('lrtp-app').surveyUrl;
          },
        },
      ],
    });
  }
}
